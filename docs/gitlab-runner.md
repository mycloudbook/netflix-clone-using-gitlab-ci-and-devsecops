# Gitlab Runner Install and Config

## Installation

### System Requirements

4 vCPU, 4 GB RAM

### Steps

1. Install the runner:

```
sudo apt update && sudo apt upgrade -y
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt-get install gitlab-runner
```

2. Register the Runner

On https://gitlab.com/ > Go to your Group > Build > Runners > New Group Runner

Get the reg token or command (choosing the 'shell' executor)

```
$ sudo gitlab-runner register
Runtime platform                                    arch=amd64 os=linux pid=2113 revision=782c6ecb version=16.9.1
Running in system-mode.                            
                                                   
Enter the GitLab instance URL (for example, https://gitlab.com/):
https://gitlab.com/
Enter the registration token:
glrt-xxxxxxxxxxxxxxxxxxxx
Verifying runner... is valid                        runner=2_exdSyfT
Enter a name for the runner. This is stored only in the local config.toml file:
[gitlab-runner-shell01]: 
Enter an executor: docker-windows, kubernetes, custom, shell, virtualbox, docker, instance, ssh, parallels, docker+machine, docker-autoscaler:
shell 
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
```

To check if it is runninng:

`sudo systemctl status gitlab-runner`


