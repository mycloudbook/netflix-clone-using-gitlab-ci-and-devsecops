# Create a Dev Instance

2 vCPU, 4 GB RAM, 30 GB SSD


## Install Docker

```
curl -fsSL https://get.docker.com -o install-docker.sh
sh install-docker.sh --dry-run
sudo sh install-docker.sh
sudo usermod -aG docker $USER
```

## Install Sonarqube

### Prerequisites

Ref: https://docs.sonarsource.com/sonarqube/latest/requirements/prerequisites-and-overview/

Set sysctl values:

```
sudo vim /etc/sysctl.d/99-sonarqube.conf

vm.max_map_count=524288
fs.file-max=131072
```

```
sudo vim /etc/security/limits.d/99-sonarqube.conf

sonarqube   -   nofile   131072
sonarqube   -   nproc    8192
```

### Start containers

Here is the Docker compose file: sonarqube/compose.yml

```
cd sonarqube
docker compose up -d
```

Access it on http://192.168.0.25:9000/
Access Adminer on http://192.168.0.25:10000/

## Configure Sonarqube

Create a new Local Project.

How do you want to analyze your repository?  Gitlab CI
Follow instructions